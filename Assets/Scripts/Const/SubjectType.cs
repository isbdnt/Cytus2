﻿namespace Cytus2
{
    public enum SubjectType
    {
        Songs,
        CurrentSong,
        CurrentChart,
    }
}