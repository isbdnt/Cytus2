﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public class GameObjectPool<T>
        where T : Component, IGameObjectPoolEntity
    {
        private Dictionary<string, List<T>> _allEntitiesMap = new Dictionary<string, List<T>>();
        private Dictionary<string, Stack<T>> _idleEntitiesMap = new Dictionary<string, Stack<T>>();
        private Dictionary<string, GameObject> _prefabMap = new Dictionary<string, GameObject>();
        private Dictionary<string, GameObject> _containerMap = new Dictionary<string, GameObject>();

        public bool HasEntityPrefab(string name)
        {
            return _prefabMap.ContainsKey(name);
        }

        public void AddEntityPrefab(string name, GameObject prefab, int size = 0)
        {
            if (_prefabMap.ContainsKey(name))
            {
                GameObject.Destroy(_containerMap[name]);
            }
            _prefabMap[name] = prefab ?? throw new Exception("Invalid prefab.");
            _allEntitiesMap[name] = new List<T>();
            _idleEntitiesMap[name] = new Stack<T>();
            _containerMap[name] = new GameObject($"GameObjectPool({typeof(T).Name}:{name})");
            if (size > 0)
            {
                ResizeContainer(name, size);
            }
        }

        public void RemoveEntityPrefab(string name)
        {
            _prefabMap.Remove(name);
            _allEntitiesMap.Remove(name);
            _idleEntitiesMap.Remove(name);
            GameObject.Destroy(_containerMap[name]);
            _containerMap.Remove(name);
        }

        public void AddEntityPrefab(GameObject prefab, int size = 0)
        {
            AddEntityPrefab("Default", prefab, size);
        }

        public T SpawnEntity(string name, Transform parent, bool inWorldSpace = true)
        {
            if (_idleEntitiesMap.TryGetValue(name, out Stack<T> idleEntities) == false)
            {
                throw new Exception($"EntityPrefab({name}) is not existed.");
            }
            T entity;
            if (idleEntities.Count > 0)
            {
                entity = idleEntities.Pop();
                entity.transform.SetParent(parent, inWorldSpace);
                entity.gameObject.SetActive(true);
                return entity;
            }
            else
            {
                entity = GameObject.Instantiate(_prefabMap[name], parent, inWorldSpace).GetComponent<T>();
                _allEntitiesMap[name].Add(entity);
            }
            return entity;
        }

        public void ResizeContainer(string name, int size)
        {
            if (_idleEntitiesMap.TryGetValue(name, out Stack<T> idleEntities) == false)
            {
                throw new Exception($"EntityPrefab({name}) is not existed.");
            }
            var allEntities = _allEntitiesMap[name];
            var prefab = _prefabMap[name];
            var container = _containerMap[name].transform;
            if (idleEntities.Count > size)
            {
                int diff = idleEntities.Count - size;
                while (diff-- > 0)
                {
                    var entity = idleEntities.Pop();
                    allEntities.Remove(entity);
                    GameObject.Destroy(entity.gameObject);
                }
            }
            else
            {
                int diff = size - idleEntities.Count;
                while (diff-- > 0)
                {
                    var entity = GameObject.Instantiate(prefab, container).GetComponent<T>();
                    entity.gameObject.SetActive(false);
                    idleEntities.Push(entity);
                    allEntities.Add(entity);
                }
            }
        }

        public T SpawnEntity(Transform parent, bool inWorldSpace = true)
        {
            return SpawnEntity("Default", parent, inWorldSpace);
        }

        public void DespawnEntity(string name, T entity)
        {
            if (_idleEntitiesMap.TryGetValue(name, out Stack<T> idleEntities))
            {
                entity.gameObject.SetActive(false);
                entity.transform.parent = _containerMap[name]?.transform;
                idleEntities.Push(entity);
            }
            else
            {
                GameObject.Destroy(entity.gameObject);
            }
        }

        public void DespawnEntity(T entity)
        {
            DespawnEntity("Default", entity);
        }

        public void DespawnAllEntities()
        {
            foreach (var allEntities in _allEntitiesMap.Values)
            {
                foreach (var entity in allEntities)
                {
                    if (entity.gameObject.activeSelf)
                    {
                        entity.Despawn();
                    }
                }
            }
        }

        public void Clear()
        {
            _idleEntitiesMap.Clear();
            _allEntitiesMap.Clear();
            _prefabMap.Clear();
            foreach (var container in _containerMap.Values)
            {
                GameObject.Destroy(container);
            }
            _containerMap.Clear();
        }
    }
}