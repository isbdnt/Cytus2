﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Cytus2
{
    public class UIView : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler, IPointerEnterHandler
    {
        public event Action onDestroy = delegate { };

        public Vector3 position
        {
            get => transform.position;
            set => transform.position = value;
        }

        public Quaternion rotation
        {
            get => transform.rotation;
            set => transform.rotation = value;
        }

        public string path { get; set; }

        public string text
        {
            get
            {
                CheckTextView();
                return _textView.text;
            }
            set
            {
                CheckTextView();
                _textView.text = value;
            }
        }

        public event Action<string> onInputChange
        {
            add
            {
                CheckInputView();
                _onInputChange += value;
            }
            remove
            {
                CheckInputView();
                _onInputChange -= value;
            }
        }

        private event Action<string> _onInputChange;

        public string input
        {
            get
            {
                CheckInputView();
                return _input;
            }
            set
            {
                CheckInputView();
                _input = value;
                _inputView.text = value;
            }
        }

        private string _input;

        public event Action<bool> onToggleChange
        {
            add
            {
                CheckToggleView();
                _onToggleChange += value;
            }
            remove
            {
                CheckToggleView();
                _onToggleChange -= value;
            }
        }

        private event Action<bool> _onToggleChange = delegate { };

        public bool toggle
        {
            get
            {
                CheckToggleView();
                return _toggle;
            }
            set
            {
                CheckToggleView();
                _toggle = value;
                _toggleView.isOn = value;
            }
        }

        private bool _toggle;

        public event Action<int> onDropdownChange
        {
            add
            {
                CheckDropdownView();
                _onDropdownChange += value;
            }
            remove
            {
                CheckDropdownView();
                _onDropdownChange -= value;
            }
        }

        private event Action<int> _onDropdownChange;

        public int dropdown
        {
            get
            {
                CheckDropdownView();
                return _dropdown;
            }
            set
            {
                CheckDropdownView();
                _dropdown = value;
                _dropdownView.value = value;
            }
        }

        private int _dropdown;

        public event Action onClick = delegate { };

        public event Action onBeginDrag = delegate { };

        public event Action<Vector2> onDrag = delegate { };

        public event Action onEndDrag = delegate { };

        public event Action onDrop = delegate { };

        public event Action onEnter = delegate { };

        public Vector2 Size
        {
            get
            {
                CheckRect();
                return _rect.sizeDelta;
            }
            set
            {
                CheckRect();
                _rect.sizeDelta = value;
            }
        }

        public bool visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }

        public Vector4 textColor
        {
            get
            {
                CheckTextView();
                return _textView.color;
            }
            set
            {
                CheckTextView();
                _textView.color = value;
            }
        }

        public Vector4 color
        {
            get
            {
                CheckImageView();
                return _imageView.color;
            }
            set
            {
                CheckImageView();
                _imageView.color = value;
            }
        }

        public Sprite sprite
        {
            get
            {
                CheckImageView();
                return _imageView.sprite;
            }
            set
            {
                CheckImageView();
                _imageView.sprite = value;
            }
        }

        public List<string> dropdownOptions
        {
            get
            {
                CheckDropdownView();
                return _dropdownView.options.ConvertAll(o => o.text);
            }
            set
            {
                CheckDropdownView();
                _dropdownView.options = value.ConvertAll(option => new TMP_Dropdown.OptionData()
                {
                    text = option,
                });
            }
        }

        public Vector2 scroll
        {
            get
            {
                CheckScrollView();
                return _scrollView.normalizedPosition;
            }
            set
            {
                CheckScrollView();
                _scrollView.normalizedPosition = value;
            }
        }

        private TMP_Text _textView;
        private Image _imageView;
        private TMP_InputField _inputView;
        private Toggle _toggleView;
        private TMP_Dropdown _dropdownView;
        private RectTransform _rect;
        private ScrollRect _scrollView;
        private Vector2 _pressPosition;

        public UIView QuerySelector(string path)
        {
            Transform child = transform.Find(path);
            if (child == null)
            {
                throw new Exception($"Node({this.path ?? "."}/{path}) not found.");
            }
            var node = child.gameObject.GetComponent<UIView>() ?? child.gameObject.AddComponent<UIView>();
            node.path = $"{this.path}/{path}";
            return node;
        }

        public T CreateNode<T>()
            where T : INode, new()
        {
            T node = new T();
            var view = GameObject.Instantiate(ResourceManager.instance.LoadPrefab(node.viewPath), transform, false).AddComponent<UIView>();
            view.path = $"{path}/{view.name}";
            node.Initialize(view);
            return node;
        }

        public UIView CreateView(UIView template)
        {
            var view = GameObject.Instantiate(template, transform, false);
            view.path = $"{path}/{view.name}";
            view.visible = true;
            return view;
        }

        public void Destroy()
        {
            GameObject.Destroy(gameObject);
        }

        private void OnDestroy()
        {
            onDestroy();
        }

        private void CheckTextView()
        {
            if (_textView == null)
            {
                _textView = GetComponent<TMP_Text>() ?? transform.Find("$Value")?.GetComponent<TMP_Text>();
                if (_textView == null)
                {
                    throw new Exception($"UINode({path}) is not a Text.");
                }
            }
        }

        private void CheckScrollView()
        {
            if (_scrollView == null)
            {
                _scrollView = GetComponent<ScrollRect>() ?? transform.Find("$Value")?.GetComponent<ScrollRect>();
                if (_scrollView == null)
                {
                    throw new Exception($"UINode({path}) is not a Scroll.");
                }
            }
        }

        private void CheckImageView()
        {
            if (_imageView == null)
            {
                _imageView = GetComponent<Image>() ?? transform.Find("$Value")?.GetComponent<Image>();
                if (_imageView == null)
                {
                    throw new Exception($"UINode({path}) is not an Image.");
                }
            }
        }

        private void CheckInputView()
        {
            if (_inputView == null)
            {
                _inputView = GetComponent<TMP_InputField>() ?? transform.Find("$Value")?.GetComponent<TMP_InputField>();
                if (_inputView == null)
                {
                    throw new Exception($"UINode({path}) is not an Input.");
                }
                _inputView.onEndEdit.AddListener(HandleInputChange);
            }
        }

        private void CheckDropdownView()
        {
            if (_dropdownView == null)
            {
                _dropdownView = GetComponent<TMP_Dropdown>() ?? transform.Find("$Value")?.GetComponent<TMP_Dropdown>();
                if (_dropdownView == null)
                {
                    throw new Exception($"UINode({path}) is not a Dropdown.");
                }
                _dropdownView.onValueChanged.AddListener(HandleDropdownChange);
            }
        }

        private void CheckRect()
        {
            if (_rect == null)
            {
                _rect = (RectTransform)transform;
            }
        }

        private void HandleDropdownChange(int dropdown)
        {
            EventSystem.current.SetSelectedGameObject(null);
            if (_dropdown != dropdown)
            {
                _dropdown = dropdown;
                _onDropdownChange(dropdown);
            }
        }

        private void HandleInputChange(string input)
        {
            if (EventSystem.current.alreadySelecting == false)
            {
                EventSystem.current.SetSelectedGameObject(null);
            }
            if (_input != input)
            {
                _input = input;
                _onInputChange(input);
            }
        }

        private void HandleToggleChange(bool toggle)
        {
            EventSystem.current.SetSelectedGameObject(null);
            if (_toggle != toggle)
            {
                _toggle = toggle;
                _onToggleChange(toggle);
            }
        }

        private void CheckToggleView()
        {
            if (_toggleView == null)
            {
                _toggleView = GetComponent<Toggle>() ?? transform.Find("$Value")?.GetComponent<Toggle>();
                if (_toggleView == null)
                {
                    throw new Exception($"UINode({path}) is not a Toggle.");
                }
                _toggleView.onValueChanged.AddListener(HandleToggleChange);
            }
        }

        public void RebuildLayout()
        {
            CheckRect();
            LayoutRebuilder.ForceRebuildLayoutImmediate(_rect);
        }

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            onClick();
        }

        void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
        {
            _pressPosition = eventData.pressPosition;
            onBeginDrag();
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            onDrag(eventData.position - _pressPosition);
        }

        void IEndDragHandler.OnEndDrag(PointerEventData eventData)
        {
            onEndDrag();
        }

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            onEnter();
        }

        public void SetRaycastTarget(bool value)
        {
            CheckImageView();
            _imageView.raycastTarget = value;
        }

        void IDropHandler.OnDrop(PointerEventData eventData)
        {
            onDrop();
        }

        public void FitToScreen()
        {
            if (_imageView.sprite.rect.width / _imageView.sprite.rect.height >= 16f / 9f)
            {
                Size = new Vector2(4000, Screen.height);
            }
            else
            {
                Size = new Vector2(Screen.width, 4000);
            }
        }

#if UNITY_EDITOR

#endif
    }
}