﻿using System;
using System.Collections.Generic;

namespace Cytus2
{
    public class GridPage : IPage
    {
        public string viewPath { get; } = "UIViews/GridPage";
        public UIView view { get; private set; }

        UIView _scoreView;
        UIView _comboView;

        public void Initialize(HistoryScene scene, UIView view)
        {
            this.view = view;
            view.QuerySelector("Pause").onClick += HandlePauseClick;
            _scoreView = view.QuerySelector("Score");
            _comboView = view.QuerySelector("Combo");

            var song = SubjectStore<SongData>.Get(SubjectType.CurrentSong);
            view.QuerySelector("SongName").text = song.name;
            var backgroundView = view.QuerySelector("Background");
            backgroundView.sprite = song.blurredCover;
            backgroundView.FitToScreen();

            new DifficultyLabelView(view.QuerySelector("DifficultyLabel")).Render(SubjectStore<ChartData>.Get(SubjectType.CurrentChart).difficulty);

            var gridView = GridView.instance;
            gridView.onComboChange += HandleGridComboChange;
            gridView.onScoreChange += HandleGridPointChange;
            gridView.onEnd += HandleSongEnd;
            _comboView.text = null;
        }

        void HandlePauseClick()
        {
            GridController.PauseGame();
            History.instance.PushPage<PausePage>();
        }

        void HandleSongEnd()
        {
            History.instance.PushPage<SummaryPage>();
        }

        void HandleGridPointChange(int score)
        {
            _scoreView.text = String.Format("{0:d7}", score);
        }

        void HandleGridComboChange(int combo)
        {
            if (combo > 0)
            {
                _comboView.text = combo.ToString();
            }
            else
            {
                _comboView.text = null;
            }
        }
    }
}