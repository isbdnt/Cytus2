﻿using System;
using System.Collections.Generic;

namespace Cytus2
{
    public class PausePage : IPage
    {
        public string viewPath { get; } = "UIViews/PausePage";
        public UIView view { get; private set; }

        private List<IDisposable> _subscriptions = new List<IDisposable>();

        public void Initialize(HistoryScene scene, UIView view)
        {
            this.view = view;
            view.QuerySelector("Menu/Resume").onClick += HandleResumeClick;
            view.QuerySelector("Menu/Retry").onClick += HandleRetryClick;
            view.QuerySelector("Menu/Leave").onClick += HandleLeaveClick;

            var song = SubjectStore<SongData>.Get(SubjectType.CurrentSong);
            view.QuerySelector("SongName").text = song.name;
            var backgroundView = view.QuerySelector("Background");
            backgroundView.sprite = song.blurredCover;
            backgroundView.FitToScreen();

            new DifficultyLabelView(view.QuerySelector("DifficultyLabel")).Render(SubjectStore<ChartData>.Get(SubjectType.CurrentChart).difficulty);

            view.QuerySelector("Score").text = String.Format("{0:d7}", GridView.instance.grid.score);
        }

        void HandleResumeClick()
        {
            History.instance.PopPage();
            GridController.ResumeGame();
        }

        void HandleRetryClick()
        {
            History.instance.PopPage();
            GridController.RestartGame();
        }

        void HandleLeaveClick()
        {
            History.instance.PopScene();
            GridController.StopGame();
        }
    }
}