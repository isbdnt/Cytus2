﻿using System;
using System.Collections.Generic;

namespace Cytus2
{
    public class PreparationPage : IPage
    {
        public string viewPath { get; } = "UIViews/PreparationPage";
        public UIView view { get; private set; }

        public void Initialize(HistoryScene scene, UIView view)
        {
            this.view = view;
            view.QuerySelector("Start").onClick += HandleStartClick;
            var backView = view.QuerySelector("Back");
            backView.onClick += History.instance.PopPage;
            backView.sprite = SubjectStore<SongData>.Get(SubjectType.CurrentSong).blurredCover;
            backView.FitToScreen();
        }

        void HandleStartClick()
        {
            History.instance.PopPage();
            GridController.StartGame();
        }
    }
}