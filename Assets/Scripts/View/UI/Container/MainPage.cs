﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public class MainPage : IPage
    {
        public string viewPath { get; } = "UIViews/MainPage";
        public UIView view { get; private set; }

        private DifficultyListView _difficultyListView;
        private List<IDisposable> _subscriptions = new List<IDisposable>();
        CoverView _coverView;
        private AudioSource _audioSource;
        SongData _song;
        UIView _songNameView;
        UIView _singerView;
        UIView _maxScoreView;

        public void Initialize(HistoryScene scene, UIView view)
        {
            this.view = view;
            _audioSource = view.GetComponent<AudioSource>();
            view.onDestroy += HandleViewDestroy;
            _songNameView = view.QuerySelector("SongName");
            _singerView = view.QuerySelector("Singer");
            _maxScoreView = view.QuerySelector("MaxScore");
            _difficultyListView = new DifficultyListView(view.QuerySelector("DifficultyList"));
            _difficultyListView.onDifficultyClick += HandleDifficultyClick;
            _coverView = new CoverView(view.QuerySelector("Cover"));
            _coverView.onDrag += HandleCoverDrag;
            _subscriptions.Add(SubjectStore<SongData>.Subscribe(SubjectType.CurrentSong, HandleCurrentSongChange));
            PlayerLoop.instance.onFixedUpdate += FixedUpdate;
        }

        void HandleDifficultyClick(ChartData chart)
        {
            SubjectStore<ChartData>.Dispatch(SubjectType.CurrentChart, chart);
            History.instance.PushPage<PreparationPage>();
        }

        void HandleCoverDrag(bool next)
        {
            var songs = SubjectStore<List<SongData>>.Get(SubjectType.Songs);
            var currentSongIndex = songs.IndexOf(_song);
            if (next)
            {
                SubjectStore<SongData>.Dispatch(SubjectType.CurrentSong, songs[(currentSongIndex + 1) % songs.Count]);
            }
            else
            {
                SubjectStore<SongData>.Dispatch(SubjectType.CurrentSong, songs[(currentSongIndex - 1 + songs.Count) % songs.Count]);
            }
        }

        private void HandleCurrentSongChange(SongData song)
        {
            _song = song;
            _difficultyListView.Render(song.charts);
            _coverView.Render(song.cover);
            _audioSource.clip = song.audioClip;
            _audioSource.time = song.featureOffset;
            _songNameView.text = song.name;
            _singerView.text = song.singer;
            _maxScoreView.text = string.Format("{0:d7}", PlayerPrefs.GetInt(song.name));
            _audioSource.Play();
        }

        void FixedUpdate()
        {
            if (_song != null && _audioSource.time > _song.featureOffset + 18f)
            {
                _audioSource.time = _song.featureOffset;
                _audioSource.Play();
            }
        }

        private void HandleViewDestroy()
        {
            foreach (var sub in _subscriptions)
            {
                sub.Dispose();
            }
        }
    }
}