﻿using System;
using System.Collections.Generic;

namespace Cytus2
{
    public class StartPage : IPage
    {
        public string viewPath { get; } = "UIViews/StartPage";
        public UIView view { get; private set; }

        public void Initialize(HistoryScene scene, UIView view)
        {
            this.view = view;
            view.onClick += HandleClick;
        }

        void HandleClick()
        {
            History.instance.PopPage();
            History.instance.PushPage<MainPage>();
        }
    }
}