﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public class SummaryPage : IPage
    {
        public string viewPath { get; } = "UIViews/SummaryPage";
        public UIView view { get; private set; }


        public void Initialize(HistoryScene scene, UIView view)
        {
            this.view = view;
            view.QuerySelector("Again").onClick += HandleAgainClick;
            view.QuerySelector("Next").onClick += HandleNextClick;

            var song = SubjectStore<SongData>.Get(SubjectType.CurrentSong);
            view.QuerySelector("SongName").text = song.name;
            var backgroundView = view.QuerySelector("Background");
            backgroundView.sprite = song.blurredCover;
            backgroundView.FitToScreen();

            new DifficultyLabelView(view.QuerySelector("DifficultyLabel")).Render(SubjectStore<ChartData>.Get(SubjectType.CurrentChart).difficulty);

            var grid = GridView.instance.grid;
            view.QuerySelector("Bar/Score").text = grid.score.ToString();
            view.QuerySelector("Bar/MaxCombo").text = grid.maxCombo.ToString();
            view.QuerySelector("BeatingResults/PerfectCount").text = grid.perfectCount.ToString();
            view.QuerySelector("BeatingResults/GoodCount").text = grid.goodCount.ToString();
            view.QuerySelector("BeatingResults/MissCount").text = grid.missCount.ToString();
            view.QuerySelector("BeatingResults/BadCount").text = grid.badCount.ToString();
            PlayerPrefs.SetInt(SubjectStore<SongData>.Get(SubjectType.CurrentSong).name, grid.score);
        }

        void HandleAgainClick()
        {
            History.instance.PopPage();
            GridController.RestartGame();
        }

        void HandleNextClick()
        {
            History.instance.PopScene();
            GridController.StopGame();
        }
    }
}