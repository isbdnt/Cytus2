﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public class History
    {
        public static History instance { get; private set; }

        private Stack<HistoryScene> _scenes = new Stack<HistoryScene>();

        private Transform _canvas;

        public static History NewInstance()
        {
            instance = new History();
            return instance;
        }

        private History()
        {
            _canvas = GameObject.Find("Canvas").transform;
            _scenes.Push(new HistoryScene(_canvas));
        }

        public bool PushPage<T>()
            where T : IPage, new()
        {
            return _scenes.Peek().PushPage<T>();
        }

        public void PushScene()
        {
            _scenes.Peek().visible = false;
            _scenes.Push(new HistoryScene(_canvas));
        }

        public void PopPage()
        {
            _scenes.Peek().PopPage();
            TryPopEmptyScene();
        }

        public void RemovePage(IPage page)
        {
            _scenes.Peek().RemovePage(page);
            TryPopEmptyScene();
        }

        void TryPopEmptyScene()
        {
            if (_scenes.Peek().pageCount == 0 && _scenes.Count > 1)
            {
                _scenes.Pop();
                _scenes.Peek().visible = true;
            }
        }

        public void PopScene()
        {
            _scenes.Peek().Destroy();
            _scenes.Pop();
            if (_scenes.Count > 0)
            {
                _scenes.Peek().visible = true;
            }
            else
            {
                _scenes.Push(new HistoryScene(_canvas));
            }
        }

        public bool HasPage(Type type)
        {
            return _scenes.Peek().HasPage(type);
        }
    }
}