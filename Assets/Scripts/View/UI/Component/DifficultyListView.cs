﻿using System;
using System.Collections.Generic;

namespace Cytus2
{
    public class DifficultyListView
    {
        public event Action<ChartData> onDifficultyClick = delegate { };

        private DifficultyView[] _difficultyViews = new DifficultyView[3];

        private UIView _view;

        public DifficultyListView(UIView view)
        {
            _view = view;
            _difficultyViews[0] = CreateDifficultyView("Easy");
            _difficultyViews[1] = CreateDifficultyView("Hard");
            _difficultyViews[2] = CreateDifficultyView("Chaos");
        }

        private DifficultyView CreateDifficultyView(string node)
        {
            var difficultyView = new DifficultyView(_view.QuerySelector(node));
            difficultyView.onClick += HandleDifficultyClick;
            return difficultyView;
        }

        public void Render(List<ChartData> charts)
        {
            for (int i = 0; i < charts.Count; i++)
            {
                _difficultyViews[i].Render(charts[i]);
            }
        }

        private void HandleDifficultyClick(ChartData chart)
        {
            onDifficultyClick(chart);
        }
    }
}