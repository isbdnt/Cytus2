﻿using System;

namespace Cytus2
{
    public class DifficultyView
    {
        public event Action<ChartData> onClick = delegate { };

        private ChartData _chart;

        public DifficultyView(UIView view)
        {
            view.onClick += HandleClick;
        }

        public void Render(ChartData chart)
        {
            _chart = chart;
        }

        private void HandleClick()
        {
            onClick(_chart);
        }
    }
}