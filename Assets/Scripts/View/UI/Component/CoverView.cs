﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public class CoverView
    {
        public event Action<bool> onDrag = delegate { };

        private UIView[] _images = new UIView[2];
        bool _draggable = true;

        public CoverView(UIView view)
        {
            view.onDrag += HandleDrag;
            view.onEndDrag += HandleEndDrag;
            _images[0] = view.QuerySelector("Image1");
            _images[1] = view.QuerySelector("Image2");
        }

        public void Render(Sprite cover)
        {
            _images[0].transform.SetAsFirstSibling();
            _images[1].sprite = cover;
            _images[1].FitToScreen();
            var temp = _images[0];
            _images[0] = _images[1];
            _images[1] = temp;
        }

        private void HandleDrag(Vector2 delta)
        {
            if (_draggable)
            {
                _draggable = false;
                if (delta.x > 0f)
                {
                    onDrag(false);
                }
                else
                {
                    onDrag(true);
                }
            }
        }

        private void HandleEndDrag()
        {
            _draggable = true;
        }
    }
}