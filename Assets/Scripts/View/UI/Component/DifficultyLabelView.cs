﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public class DifficultyLabelView
    {
        public event Action<bool> onDrag = delegate { };

        private UIView _easyView;
        private UIView _hardView;
        private UIView _chaosView;

        public DifficultyLabelView(UIView view)
        {
            _easyView = view.QuerySelector("Easy");
            _hardView = view.QuerySelector("Hard");
            _chaosView = view.QuerySelector("Chaos");
        }

        public void Render(ChartDifficultyType difficultyType)
        {
            switch (difficultyType)
            {
                case ChartDifficultyType.Easy:
                    _easyView.visible = true;
                    _hardView.visible = false;
                    _chaosView.visible = false;
                    break;
                case ChartDifficultyType.Hard:
                    _easyView.visible = false;
                    _hardView.visible = true;
                    _chaosView.visible = false;
                    break;
                case ChartDifficultyType.Chaos:
                    _easyView.visible = false;
                    _hardView.visible = false;
                    _chaosView.visible = true;
                    break;
                default:
                    break;
            }
        }
    }
}