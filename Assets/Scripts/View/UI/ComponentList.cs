﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public class ComponentList<T>
    {
        public int count => _activeCount;

        public T this[int i]
        {
            get
            {
                if (i >= _activeCount)
                {
                    throw new Exception("Index out of range.");
                }
                return _components[i];
            }
        }

        private List<UIView> _nodes = new List<UIView>();
        private List<T> _components = new List<T>();

        private UIView _template;
        private int _activeCount;

        private Func<string, T> _createComponent;

        public ComponentList(UIView template, Func<string, T> createComponent)
        {
            _template = template;
            _createComponent = createComponent;
            template.gameObject.SetActive(false);
        }

        public void Resize(int size)
        {
            if (size < _activeCount)
            {
                for (int i = size; i < _activeCount; i++)
                {
                    _nodes[i].gameObject.SetActive(false);
                }
                _activeCount = size;
            }
            else if (size > _activeCount)
            {
                if (size > _nodes.Capacity)   // Optimization
                {
                    _nodes.Capacity = size;
                }
                if (size > _components.Capacity)
                {
                    _components.Capacity = size;
                }
                for (int i = _activeCount; i < size; i++)
                {
                    if (i < _nodes.Count)
                    {
                        _nodes[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        var node = GameObject.Instantiate(_template.gameObject, _template.transform.parent, false).GetComponent<UIView>();
                        node.name = $"{_template.gameObject.name}({i})";
                        node.gameObject.SetActive(true);
                        _nodes.Add(node);
                        _components.Add(_createComponent(node.name));
                    }
                }
                _activeCount = size;
            }
        }
    }
}