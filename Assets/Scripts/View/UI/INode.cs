﻿namespace Cytus2
{
    public interface INode
    {
        string viewPath { get; }
        UIView view { get; }

        void Initialize(UIView view);
    }
}