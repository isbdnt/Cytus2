﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public class HistoryScene
    {
        public int pageCount => _pages.Count;
        public bool visible
        {
            get => _visible;
            set
            {
                _visible = value;
                foreach (var page in _pages)
                {
                    page.view.visible = value;
                }
            }
        }

        bool _visible;
        private List<IPage> _pages = new List<IPage>();
        private HashSet<Type> _pageExistenceSet = new HashSet<Type>();

        private Transform _view;

        public HistoryScene(Transform view)
        {
            _view = view;
        }

        public bool PushPage<T>()
            where T : IPage, new()
        {
            Type type = typeof(T);
            if (_pageExistenceSet.Contains(type) == false)
            {
                IPage newPage = new T();
                GameObject prefab = ResourceManager.instance.LoadPrefab(newPage.viewPath);
                if (prefab == null)
                {
                    throw new Exception($"Page({newPage.viewPath}) not found.");
                }
                UIView newView = GameObject.Instantiate(prefab, _view, false).AddComponent<UIView>();
                newView.path = newPage.viewPath;
                newPage.Initialize(this, newView);
                _pages.Add(newPage);
                _pageExistenceSet.Add(type);
                return true;
            }
            return false;
        }

        public void PopPage()
        {
            if (_pages.Count > 0)
            {
                IPage currentPage = _pages[_pages.Count - 1];
                _pages.RemoveAt(_pages.Count - 1);
                currentPage.view.Destroy();
                _pageExistenceSet.Remove(currentPage.GetType());
            }
        }

        public void RemovePage(IPage page)
        {
            if (_pages.Count > 0)
            {
                _pages.Remove(page);
                page.view.Destroy();
                _pageExistenceSet.Remove(page.GetType());
            }
        }

        public bool HasPage(Type type)
        {
            return _pageExistenceSet.Contains(type);
        }

        public void Destroy()
        {
            foreach (var page in _pages)
            {
                page.view.Destroy();
            }
        }
    }
}