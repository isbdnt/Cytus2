﻿namespace Cytus2
{
    public interface IPage
    {
        string viewPath { get; }
        UIView view { get; }

        void Initialize(HistoryScene scene, UIView view);
    }
}