﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public class GridView : MonoBehaviour
    {
        public static GridView instance { get; private set; }
        public static GameObject prefab { get; set; }

        public event Action<int> onScoreChange = delegate { };

        public event Action<int> onComboChange = delegate { };

        public event Action onStart = delegate { };

        public event Action onContinue = delegate { };

        public event Action onPause = delegate { };

        public event Action onEnd = delegate { };

        public GridViewAnchor anchor { get; private set; }
        public Transform beatingResultContainer { get; private set; }
        public Vector3 scanLinePosition => _scanLine.transform.position;
        public int scanLineDirection { get; private set; }
        public Grid grid => _grid;

        private GameObject _scanLinePrefab;
        private GameObject _borderPrefab;

        private AudioSource _audioSource;

        private Transform _noteContainer;
        public int cellSize => _cellSize;
        public bool playing => _playing;

        [SerializeField]
        private int _cellSize;

        private Grid _grid;
        private Dictionary<Note, NoteView> _noteViewMap = new Dictionary<Note, NoteView>();
        private float _stepLength;
        public float turnLength { get; private set; }
        private float _turnTimeOffset;
        private GameObject _scanLine;
        private SongData _songData;
        private ChartData _chartData;

        private bool _playing;
        private int _currentTurn;
        private Vector3 _top;
        private Vector3 _bottom;
        private GameObject _topBorder;
        private GameObject _bottomBorder;
        private float _startTime;

        public static GridView NewInstance()
        {
            if (instance != null)
            {
                instance.Destroy();
            }
            instance = GameObject.Instantiate(prefab, GameObject.Find("Canvas").transform, false).GetComponent<GridView>();
            instance.transform.SetAsFirstSibling();
            return instance;
        }

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();

            //todo: Move ScanLine into ResourceManager
            _scanLinePrefab = Resources.Load<GameObject>("Prefabs/ScanLine");
            _borderPrefab = Resources.Load<GameObject>("Prefabs/Border");
            _topBorder = GameObject.Instantiate(_borderPrefab, transform, false);
            _bottomBorder = GameObject.Instantiate(_borderPrefab, transform, false);
            _scanLine = GameObject.Instantiate(_scanLinePrefab, transform, false);

            _noteContainer = new GameObject("NoteContainer").transform;
            _noteContainer.SetParent(transform, false);
            beatingResultContainer = new GameObject("BeatingResultContainer").transform;
            beatingResultContainer.SetParent(transform, false);
        }

        public void Initialize(SongData songData, ChartData chartData)
        {
            float beatLength = 60f / songData.bpm;
            turnLength = beatLength * songData.beatUnit / chartData.beatUnit;
            _stepLength = turnLength / 16f;
            _turnTimeOffset = turnLength - beatLength * 0.125f;
            _audioSource.clip = songData.audioClip;
            _songData = songData;
            _chartData = chartData;
            ResetState();
        }

        private void ResetState()
        {
            _grid = new Grid(_chartData, 2 / (_songData.beatUnit / _chartData.beatUnit));
            _grid.onAddNote += HandleGridAddNote;
            _grid.onRemoveNote += HandleGridRemoveNote;
            _grid.onScoreChange += HandleGridScoreChange;
            _grid.onComboChange += HandleGridComboChange;
            HandleGridScoreChange(0);
            HandleGridComboChange(0);
            scanLineDirection = _songData.upsideDown ? 1 : -1;
            anchor = new GridViewAnchor(16, 8, _cellSize, new Vector2Int(8, 0), _songData.upsideDown);
            _top = anchor.ToWorldPosition(new Vector2Int(0, 8));
            _bottom = anchor.ToWorldPosition(new Vector2Int(0, 0));
            _playing = false;
            _scanLine.SetActive(false);
            _scanLine.transform.position = Vector3.Lerp(_top, _bottom, _turnTimeOffset % turnLength / turnLength);
            _currentTurn = 0;
            _topBorder.transform.position = _top;
            _bottomBorder.transform.position = _bottom;
            _audioSource.time = _songData.playOffset;
            _noteViewMap.Clear();
            _scanLine.transform.SetAsLastSibling();
            NoteView.pool.DespawnAllEntities();
            ClickRhythmView.pool.DespawnAllEntities();
            HoldRhythmView.pool.DespawnAllEntities();
            SpecialHoldRhythmView.pool.DespawnAllEntities();
            DragRhythmView.pool.DespawnAllEntities();
            FlickRhythmView.pool.DespawnAllEntities();
            BeatingResultView.pool.DespawnAllEntities();
        }

        private void HandleGridScoreChange(int point)
        {
            onScoreChange(point);
        }

        private void HandleGridComboChange(int combo)
        {
            onComboChange(combo);
        }

        public void StartGame()
        {
            if (_playing == false)
            {
                if (Time.time - _startTime >= _audioSource.clip.length)
                {
                    ResetState();
                }
                _playing = true;
                _scanLine.SetActive(true);
                _audioSource.Play();
                _startTime = Time.time - _audioSource.time;
                Time.timeScale = 1f;
                if (_audioSource.time == _songData.playOffset)
                {
                    onStart();
                }
                else
                {
                    onContinue();
                }
            }
        }

        public void RestartGame()
        {
            ResetState();
            StartGame();
        }

        public void PauseGame()
        {
            if (_playing)
            {
                _playing = false;
                _audioSource.Pause();
                Time.timeScale = 0f;
                onPause();
            }
        }

        void EndGame()
        {
            _playing = false;
            _audioSource.Pause();
            onEnd();
        }

        private void FixedUpdate()
        {
            if (_playing)
            {
                float time = Time.time - _startTime - _songData.playOffset;
                float turnTime = time + _turnTimeOffset;
                float currentStep = time / _stepLength;
                _grid.Update(currentStep);
                int currentTurn = Mathf.FloorToInt(turnTime / turnLength);
                if (currentTurn > _currentTurn)
                {
                    _currentTurn = currentTurn;
                    UpsideDown();
                }
                _scanLine.transform.position = Vector3.Lerp(_top, _bottom, turnTime % turnLength / turnLength);
                foreach (var noteView in _noteViewMap.Values)
                {
                    noteView.Render(currentStep);
                }
                if (time + _songData.playOffset >= _audioSource.clip.length)
                {
                    EndGame();
                }
            }
        }

        private void UpsideDown()
        {
            var temp = _top;
            _top = _bottom;
            _bottom = temp;
            scanLineDirection *= -1;
        }

        private void HandleGridAddNote(Note note)
        {
            NoteView noteView = NoteView.pool.SpawnEntity(_noteContainer, false);
            noteView.Initialize(note);
            noteView.onDestroy += HandleNoteViewDestroy;
            _noteViewMap[note] = noteView;
        }

        private void HandleNoteViewDestroy(NoteView noteView)
        {
            _noteViewMap.Remove(noteView.note);
        }

        private void HandleGridRemoveNote(Note note)
        {
        }

        private void OnDestroy()
        {
            _grid.onAddNote -= HandleGridAddNote;
            _grid.onRemoveNote -= HandleGridRemoveNote;
            _grid.onScoreChange -= HandleGridScoreChange;
            _grid.onComboChange -= HandleGridComboChange;
            NoteView.pool.DespawnAllEntities();
            ClickRhythmView.pool.DespawnAllEntities();
            HoldRhythmView.pool.DespawnAllEntities();
            SpecialHoldRhythmView.pool.DespawnAllEntities();
            DragRhythmView.pool.DespawnAllEntities();
            FlickRhythmView.pool.DespawnAllEntities();
            BeatingResultView.pool.DespawnAllEntities();
        }

        public void Destroy()
        {
            Time.timeScale = 1f;
            GameObject.Destroy(instance.gameObject);
            instance = null;
        }
    }
}