﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public class ResourceManager
    {
        public static ResourceManager instance { get; private set; }

        public Dictionary<int, SongData> songDataMap { get; private set; } = new Dictionary<int, SongData>();
        private Dictionary<string, GameObject> _prefabMap = new Dictionary<string, GameObject>();

        public static ResourceManager NewInstance()
        {
            if (instance != null)
            {
                instance.songDataMap.Clear();
                GridView.prefab = null;
                NoteView.pool.Clear();
                ClickRhythmView.pool.Clear();
                HoldRhythmView.pool.Clear();
                SpecialHoldRhythmView.pool.Clear();
                DragRhythmView.pool.Clear();
                FlickRhythmView.pool.Clear();
                BeatingResultView.pool.Clear();
                Resources.UnloadUnusedAssets();
            }
            instance = new ResourceManager();
            return instance;
        }

        private ResourceManager()
        {
            LoadSong("OneWayLove");
            LoadSong("SdoricaTheStoryUnfolds");
            LoadSong("BodyTalk");
            LoadPrefabs();
        }

        private void LoadSong(string name)
        {
            var song = JsonConvert.DeserializeObject<SongData>(Resources.Load<TextAsset>($"Songs/{name}/Config").text);
            song.audioClip = Resources.Load<AudioClip>($"Songs/{name}/Soundtrack");
            song.cover = Resources.Load<Sprite>($"Songs/{name}/Cover");
            song.blurredCover = Resources.Load<Sprite>($"Songs/{name}/BlurredCover");
            songDataMap[song.id] = song;
        }

        private void LoadPrefabs()
        {
            GridView.prefab = Resources.Load<GameObject>("Prefabs/Grid");
            NoteView.pool.AddEntityPrefab(Resources.Load<GameObject>("Prefabs/Note"), 5);
            ClickRhythmView.pool.AddEntityPrefab(Resources.Load<GameObject>("Prefabs/ClickRhythm"), 5);
            HoldRhythmView.pool.AddEntityPrefab(Resources.Load<GameObject>("Prefabs/HoldRhythm"), 5);
            SpecialHoldRhythmView.pool.AddEntityPrefab(Resources.Load<GameObject>("Prefabs/SpecialHoldRhythm"), 5);
            DragRhythmView.pool.AddEntityPrefab(Resources.Load<GameObject>("Prefabs/DragRhythm"), 5);
            FlickRhythmView.pool.AddEntityPrefab(Resources.Load<GameObject>("Prefabs/FlickRhythm"), 5);
            BeatingResultView.pool.AddEntityPrefab("Good", Resources.Load<GameObject>("Prefabs/GoodBeating"), 5);
            BeatingResultView.pool.AddEntityPrefab("Perfect", Resources.Load<GameObject>("Prefabs/PerfectBeating"), 5);
            BeatingResultView.pool.AddEntityPrefab("Miss", Resources.Load<GameObject>("Prefabs/MissBeating"), 5);
            BeatingResultView.pool.AddEntityPrefab("Bad", Resources.Load<GameObject>("Prefabs/BadBeating"), 5);
        }

        public GameObject LoadPrefab(string path)
        {
            if (_prefabMap.TryGetValue(path, out GameObject prefab))
            {
                return prefab;
            }
            prefab = Resources.Load<GameObject>(path);
            _prefabMap[path] = prefab ?? throw new Exception($"Prefab({path}) not found.");
            return prefab;
        }
    }
}