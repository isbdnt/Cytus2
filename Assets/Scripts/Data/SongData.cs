﻿using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public class SongData
    {
        public int id;
        public string name;
        public string singer;
        public int bpm;//beats per minute
        public int beatUnit;//beat unit of time signature
        public float playOffset;
        public float featureOffset;
        public bool upsideDown;
        public List<ChartData> charts;
        public AudioClip audioClip;
        public Sprite cover;
        public Sprite blurredCover;
    }
}