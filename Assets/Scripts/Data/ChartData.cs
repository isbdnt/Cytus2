﻿namespace Cytus2
{
    public class ChartData
    {
        public int level;
        public ChartDifficultyType difficulty;
        public int beatUnit;//beat unit of turn
        public NoteData[] notes;
    }
}