﻿using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public static class Main
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void TestOneWayLove()
        {
            Time.fixedDeltaTime = 1f / 60f;
            QualitySettings.vSyncCount = 1;
            Application.targetFrameRate = 60;
            History.NewInstance();
            PlayerLoop.NewInstance();
            ResourceManager.NewInstance();
            History.instance.PushPage<StartPage>();
            var songs = new List<SongData>()
            {
                ResourceManager.instance.songDataMap[1],
                ResourceManager.instance.songDataMap[2],
                ResourceManager.instance.songDataMap[3],
            };
            SubjectStore<List<SongData>>.Dispatch(SubjectType.Songs, songs);
            foreach (var song in songs)
            {
                if (PlayerPrefs.HasKey(song.name) == false)
                {
                    PlayerPrefs.SetInt(song.name, 0);
                }
            }
            SubjectStore<SongData>.Dispatch(SubjectType.CurrentSong, ResourceManager.instance.songDataMap[1]);
        }
    }
}