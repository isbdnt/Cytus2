﻿namespace Cytus2
{
    public class SpecialHoldRhythm : HoldRhythm
    {
        public SpecialHoldRhythm(Note note, RhythmData data, int stepOffset) : base(note, data, stepOffset)
        {
        }
    }
}