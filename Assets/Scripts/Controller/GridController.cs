﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Cytus2
{
    public static class GridController 
    {
        public static void StartGame()
        {
            GridView gridView = GridView.NewInstance();
            gridView.Initialize(SubjectStore<SongData>.Get(SubjectType.CurrentSong), SubjectStore<ChartData>.Get(SubjectType.CurrentChart));
            gridView.StartGame();
            History.instance.PushScene();
            History.instance.PushPage<GridPage>();
        }

        public static void PauseGame()
        {
            GridView.instance.PauseGame();
        }

        public static void ResumeGame()
        {
            GridView.instance.StartGame();
        }

        public static void RestartGame()
        {
            GridView.instance.RestartGame();
        }

        public static void StopGame()
        {
            GridView.instance.Destroy();
            SubjectStore<SongData>.Dispatch(SubjectType.CurrentSong, SubjectStore<SongData>.Get(SubjectType.CurrentSong));
        }
    }
}