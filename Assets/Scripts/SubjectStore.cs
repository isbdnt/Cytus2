﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Cytus2
{
    public class SubjectStore<T> : MonoBehaviour
    {
        private static Dictionary<SubjectType, BehaviorSubject<T>> _subjectMap = new Dictionary<SubjectType, BehaviorSubject<T>>();

        public static IDisposable Subscribe(SubjectType subject, Action<T> observer)
        {
            if (_subjectMap.TryGetValue(subject, out BehaviorSubject<T> bs) == false)
            {
                bs = new BehaviorSubject<T>(default);
                _subjectMap[subject] = bs;
            }
            return bs.Where(v => v != null).Subscribe(observer);
        }

        public static void Dispatch(SubjectType subject, T value)
        {
            if (_subjectMap.TryGetValue(subject, out BehaviorSubject<T> bs) == false)
            {
                bs = new BehaviorSubject<T>(default);
                _subjectMap[subject] = bs;
            }
            bs.OnNext(value);
        }

        public static T Get(SubjectType subject)
        {
            if (_subjectMap.TryGetValue(subject, out BehaviorSubject<T> bs))
            {
                return bs.Value;
            }
            return default;
        }

        public static void Clear()
        {
            _subjectMap.Clear();
        }
    }
}